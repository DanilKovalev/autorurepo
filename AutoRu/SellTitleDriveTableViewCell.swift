//
//  SellTitleDriveTableViewCell.swift
//  AutoRu
//
//  Created by Quaka on 29.07.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit
import TTSegmentedControl

class SellTitleDriveTableViewCell: UITableViewCell {

    @IBOutlet weak var leftLineView: UIView!
    @IBOutlet weak var rightLineView: UIView!
    @IBOutlet weak var driveUnitLabel: UILabel!
    @IBOutlet weak var driveUnitView: UIView!
    @IBOutlet weak var selectDriveUnitView: TTSegmentedControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectDriveUnitView.allowChangeThumbWidth = false
        selectDriveUnitView.defaultTextFont = UIFont.systemFont(ofSize: 16)
        selectDriveUnitView.selectedTextFont = UIFont.systemFont(ofSize: 16)
        selectDriveUnitView.containerBackgroundColor = .clear
        selectDriveUnitView.layer.borderColor = UIColor(netHex: 0xf1f1f1).cgColor
        selectDriveUnitView.layer.borderWidth = 0.8
        selectDriveUnitView.layer.cornerRadius = 5
        selectDriveUnitView.thumbColor = UIColor(netHex: 0xf1f1f1)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
