//
//  SellCtcDataTableViewCell.swift
//  AutoRu
//
//  Created by Quaka on 29.07.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

class SellCtcDataTableViewCell: UITableViewCell {

    @IBOutlet weak var ctcLabel: UILabel!
    @IBOutlet weak var leftCtcLineView: UIView!
    @IBOutlet weak var rightCtcLineView: UIView!
    @IBOutlet weak var ctcDataTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        ctcDataTextField.borderStyle = .none
        ctcDataTextField.layer.borderColor = UIColor(netHex: 0xf1f1f1).cgColor
        ctcDataTextField.layer.borderWidth = 0.8
        ctcDataTextField.layer.cornerRadius = 2

        let dataTextFields = UITextField(frame: CGRect(x: 0, y: 0, width: 200, height: 30))
        ctcDataTextField.attributedPlaceholder = NSAttributedString(string: "Ввести CTC номер",
                                                                 attributes: [NSForegroundColorAttributeName: UIColor(red: 0, green: 0.7647, blue: 0.9569, alpha: 1.0)])
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
