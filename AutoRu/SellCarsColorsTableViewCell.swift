//
//  CarsColorsTableViewCell.swift
//  AutoRu
//
//  Created by Quaka on 29.07.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

class SellCarsColorsTableViewCell: UITableViewCell {

    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var leftColorView: UIView!
    @IBOutlet weak var rightColorView: UIView!
    @IBOutlet weak var carsColorImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
