//
//  SellEntryDataVinAndSTSTableViewCell.swift
//  AutoRu
//
//  Created by Quaka on 29.07.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

class SellEntryDataVinAndSTSTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameDataLabel: UILabel!
    @IBOutlet weak var leftDataView: UIView!
    @IBOutlet weak var rightDataView: UIView!
    @IBOutlet weak var dataTextField: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        dataTextField.borderStyle = .none
        dataTextField.layer.borderColor = UIColor(netHex: 0xf1f1f1).cgColor
        dataTextField.layer.borderWidth = 0.8
        dataTextField.layer.cornerRadius = 2
        
        let dataTextFields = UITextField(frame: CGRect(x: 0, y: 0, width: 200, height: 30))
        dataTextField.attributedPlaceholder = NSAttributedString(string: "Ввести VIN номер",
                                                               attributes: [NSForegroundColorAttributeName: UIColor.red])
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
