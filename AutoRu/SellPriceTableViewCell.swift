//
//  SellPriceTableViewCell.swift
//  AutoRu
//
//  Created by Quaka on 30.07.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

class SellPriceTableViewCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var rightLineView: UIView!
    @IBOutlet weak var leftLineView: UIView!
    @IBOutlet weak var priceTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        priceTextField.borderStyle = .none
        priceTextField.layer.borderColor = UIColor(netHex: 0xf1f1f1).cgColor
        priceTextField.layer.borderWidth = 0.8
        priceTextField.layer.cornerRadius = 2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
