//
//  SellSelectPhotoTableViewCell.swift
//  AutoRu
//
//  Created by Quaka on 30.07.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

class SellSelectPhotoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var selectPhotoView: UIView!
    @IBOutlet weak var uploadPhotoButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        

//        selectPhotoView.allowChangeThumbWidth = false
//        selectPhotoView.defaultTextFont = UIFont.systemFont(ofSize: 16)
//        selectPhotoView.selectedTextFont = UIFont.systemFont(ofSize: 16)
//        selectPhotoView.containerBackgroundColor = .clear
        uploadPhotoButton.layer.borderColor = UIColor(netHex: 0xf1f1f1).cgColor
        uploadPhotoButton.layer.borderWidth = 0.8
        uploadPhotoButton.layer.cornerRadius = 5
//        selectPhotoView.thumbColor = UIColor(netHex: 0xf1f1f1)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
