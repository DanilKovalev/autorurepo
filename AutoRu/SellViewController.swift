//
//  ViewController.swift
//  AutoRu
//
//  Created by Quaka on 29.07.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import TTSegmentedControl

class SellViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var sellTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        sellTableView.backgroundColor = UIColor(netHex: 0xf1f1f1)
        sellTableView.separatorInset = UIEdgeInsets(top: 0, left: 15000, bottom: 0, right: 0)
        
        sellTableView.delegate   = self
        sellTableView.dataSource = self

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = UITableViewCell()
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameMachineCell", for: indexPath) as! SellNameMachineTableViewCell
            cell.selectionStyle = .none
            cell.machineTextField.keyboardType = .default
            cell.machineTextField.autocorrectionType = .no
            cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            cell.machineTextField.text = ""
            cell.machineTextField.placeholder = "Mercedes-Benz"
            cell.machineLabel?.textColor = UIColor.lightGray
            return cell
        } else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "modelCell", for: indexPath) as! SellModelTableViewCell
            cell.selectionStyle = .none
            cell.modelTextField.keyboardType = .default
            cell.modelTextField.autocorrectionType = .no
            cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            cell.modelTextField.placeholder = "C180"
            cell.modelLabel?.textColor = UIColor.lightGray
            return cell
        } else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "modelCell", for: indexPath) as! SellModelTableViewCell
            cell.selectionStyle = .none
            cell.modelLabel.text = "Год выпуска"
            cell.modelTextField.keyboardType = .default
            cell.modelTextField.autocorrectionType = .no
            cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            cell.modelTextField.placeholder = "1990"
            cell.modelLabel?.textColor = UIColor.lightGray
            return cell
        } else if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "modelCell", for: indexPath) as! SellModelTableViewCell
            cell.selectionStyle = .none
            cell.modelLabel.text = "Поколение"
            cell.modelTextField.keyboardType = .default
            cell.modelTextField.autocorrectionType = .no
            cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            cell.modelTextField.placeholder = "III (W204) Рестайлинг"
            cell.modelLabel?.textColor = UIColor.lightGray
            return cell
        } else if indexPath.row == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameMachineCell", for: indexPath) as! SellNameMachineTableViewCell
            cell.selectionStyle = .none
            cell.machineTextField.keyboardType = .default
            cell.machineTextField.autocorrectionType = .no
            cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            cell.machineLabel.text = "Тип кузова"
            cell.machineImageView.image = #imageLiteral(resourceName: "bentley")
            cell.machineTextField.text = ""
            cell.machineTextField.placeholder = "седан"
            cell.machineLabel?.textColor = UIColor.lightGray
            return cell
        } else if indexPath.row == 6 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "modelCell", for: indexPath) as! SellModelTableViewCell
            cell.selectionStyle = .none
            cell.modelLabel.text = "Модификация"
            cell.modelTextField.keyboardType = .default
            cell.modelTextField.autocorrectionType = .no
            cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            cell.modelTextField.placeholder = "С180 1,8 АТ (156 л.с.)"
            cell.modelLabel?.textColor = UIColor.lightGray
            return cell
        } else if indexPath.row == 7 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "chooseFuelCell", for: indexPath) as! SellFuelTypeTableViewCell
            cell.segmentedControlView.itemTitles = ["Бензин", "Дизель"]
            return cell
        } else if indexPath.row == 8 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleDriveCell", for: indexPath) as! SellTitleDriveTableViewCell
            cell.selectDriveUnitView.itemTitles = ["Передний", "Задний", "Полный"]
            cell.driveUnitLabel.textColor = UIColor.darkGray
            cell.leftLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.rightLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            return cell
        } else if indexPath.row == 9 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleDriveCell", for: indexPath) as! SellTitleDriveTableViewCell
            cell.selectDriveUnitView.itemTitles = ["Автомат", "Механика"]
            cell.driveUnitLabel.textColor = UIColor.darkGray
            cell.driveUnitLabel.text = "КПП"
            cell.leftLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.rightLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            return cell
        } else if indexPath.row == 10 {
            let cell = UITableViewCell()
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            return cell
        } else if indexPath.row == 11 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleDriveCell", for: indexPath) as! SellTitleDriveTableViewCell
            cell.selectDriveUnitView.itemTitles = ["Новый", "Ввести"]
            cell.driveUnitLabel.textColor = UIColor.darkGray
            cell.driveUnitLabel.text = "Пробег"
            cell.leftLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.rightLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            return cell
        } else if indexPath.row == 12 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "carsColorCell", for: indexPath) as! SellCarsColorsTableViewCell
            cell.colorLabel.text = "Цвета"
            return cell
            // Цвета машин
        } else if indexPath.row == 13 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleDriveCell", for: indexPath) as! SellTitleDriveTableViewCell
            cell.selectDriveUnitView.itemTitles = ["Отличное", "Хорошее", "Среднее", "Битый"]
            cell.driveUnitLabel.textColor = UIColor.darkGray
            cell.driveUnitLabel.text = "Состояние"
            cell.leftLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.rightLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            return cell
        } else if indexPath.row == 14 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleDriveCell", for: indexPath) as! SellTitleDriveTableViewCell
            cell.selectDriveUnitView.itemTitles = ["Да", "Нет"]
            cell.driveUnitLabel.textColor = UIColor.darkGray
            cell.driveUnitLabel.text = "На гарантии?"
            cell.leftLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.rightLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            return cell
        } else if indexPath.row == 15 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "documentsNameCell", for: indexPath) as! SellDocumentsNameTableViewCell
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            cell.documentLabel.textColor = UIColor.darkGray
            return cell
        } else if indexPath.row == 16 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleDriveCell", for: indexPath) as! SellTitleDriveTableViewCell
            cell.selectDriveUnitView.itemTitles = ["Оригинал", "Дубликат"]
            cell.driveUnitLabel.textColor = UIColor.darkGray
            cell.driveUnitLabel.text = "ПТС"
            cell.leftLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.rightLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            return cell
        } else if indexPath.row == 17 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleDriveCell", for: indexPath) as! SellTitleDriveTableViewCell
            cell.selectDriveUnitView.itemTitles = ["1", "2", "3", "4"]
            cell.driveUnitLabel.textColor = UIColor.darkGray
            cell.driveUnitLabel.text = "Владельцев по ПТС"
            cell.leftLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.rightLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            return cell
        } else if indexPath.row == 18 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "entryVinAndStsCell", for: indexPath) as! SellEntryDataVinAndSTSTableViewCell
            cell.dataTextField.placeholder = "Ввести VIN номер"
            cell.dataTextField.textColor = UIColor.red
            cell.leftDataView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.rightDataView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            return cell
        } else if indexPath.row == 19 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ctcDataCell", for: indexPath) as! SellCtcDataTableViewCell
            cell.ctcDataTextField.textColor = UIColor(red: 0, green: 0.9608, blue: 0.9765, alpha: 1.0)
            cell.leftCtcLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.rightCtcLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            return cell
        } else if indexPath.row == 20 {
            let cell = UITableViewCell()
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            return cell
        } else if indexPath.row == 21 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectPhotoCell", for: indexPath) as! SellSelectPhotoTableViewCell
            return cell
        } else if indexPath.row == 22 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "documentsNameCell", for: indexPath) as! SellDocumentsNameTableViewCell
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            cell.documentLabel.textColor = UIColor.darkGray
            cell.documentLabel.text = ""
            return cell
        } else if indexPath.row == 23 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "priceCell", for: indexPath) as! SellPriceTableViewCell
            cell.priceTextField.placeholder = "Ввести цену"
            cell.priceTextField.textColor = UIColor.black
            cell.leftLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.rightLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            return cell
        } else if indexPath.row == 24 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "priceCell", for: indexPath) as! SellPriceTableViewCell
            cell.priceTextField.placeholder = "Ввести номер телефона"
            cell.priceTextField.textColor = UIColor.black
            cell.priceLabel.text = "Номер телефона"
            cell.priceLabel.textColor = UIColor(red: 0.0275, green: 0.8392, blue: 0, alpha: 1.0)
            cell.leftLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.rightLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            return cell
        } else if indexPath.row == 25 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "priceCell", for: indexPath) as! SellPriceTableViewCell
            cell.priceTextField.placeholder = "Константин"
            cell.priceLabel.text = "Контактное лицо"
            cell.priceTextField.textColor = UIColor.black
            cell.leftLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            cell.rightLineView.backgroundColor = UIColor(netHex: 0xf1f1f1)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < 7 || indexPath.row == 10 || indexPath.row == 15 {
            return 44
        } else if indexPath.row == 7 {
            return 55
        } else if indexPath.row > 7 && indexPath.row < 10 || indexPath.row > 10 && indexPath.row < 15 || indexPath.row > 15 && indexPath.row < 20 || indexPath.row > 22 && indexPath.row < 26 {
            return 79
        } else if indexPath.row == 20 || indexPath.row == 22 {
            return 10
        } else if indexPath.row == 21 {
            return 170
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 26
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

