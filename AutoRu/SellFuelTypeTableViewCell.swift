//
//  sellFuelTypeTableViewCell.swift
//  AutoRu
//
//  Created by Quaka on 29.07.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit
import TTSegmentedControl

class SellFuelTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var chooseFuelView: UIView!
    @IBOutlet weak var segmentedControlView: TTSegmentedControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        segmentedControlView.allowChangeThumbWidth = false
        segmentedControlView.defaultTextFont = UIFont.systemFont(ofSize: 16)
        segmentedControlView.selectedTextFont = UIFont.systemFont(ofSize: 16)
        segmentedControlView.containerBackgroundColor = .clear
        segmentedControlView.layer.borderColor = UIColor(netHex: 0xf1f1f1).cgColor
        segmentedControlView.layer.borderWidth = 0.8
        segmentedControlView.layer.cornerRadius = 5
        segmentedControlView.thumbColor = UIColor(netHex: 0xf1f1f1)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
