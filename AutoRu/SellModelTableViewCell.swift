//
//  ModelTableViewCell.swift
//  AutoRu
//
//  Created by Quaka on 29.07.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

class SellModelTableViewCell: UITableViewCell {
    
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var modelTextField: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
